import {generateSVG} from './qr.js';

export class QRCodeElement extends HTMLElement {

    static get observedAttributes() {
        return ['data', 'ecc-level', 'margin', 'mask', 'mode', 'version'];
    }

    constructor() {
        super();
        this.attachShadow({mode: 'open'});
        this.shadowRoot.innerHTML = `
<style>
:host {
    display: inline-block;
}
:host([hidden]) {
    display: none
}
svg {
    display: block;
}
</style>
`;
    }

    attributeChangedCallback() {
        const old = this.shadowRoot.querySelector('svg');
        if (old) {
            old.remove();
        }

        const data = this.getAttribute('data');
        if (data) {
            const options = {};
            if (this.hasAttribute('ecc-level')) {
                options.ecclevel = this.getAttribute('ecc-level');
            }
            if (this.hasAttribute('margin')) {
                options.margin = this.getAttribute('margin');
            }
            if (this.hasAttribute('mask')) {
                options.mask = this.getAttribute('mask');
            }
            if (this.hasAttribute('mode')) {
                options.mode = this.getAttribute('mode');
            }
            if (this.hasAttribute('version')) {
                options.version = this.getAttribute('version');
            }
            this.shadowRoot.append(generateSVG(data, options));
        }
    }
}

window.customElements.define('qr-code', QRCodeElement);
